# Atelier reproductibilité

Trois exercices sont proposés, à faire selon ses préférences, sans ordre particulier.

## Création d'une recette Conda

Suivre [ce tutoriel](https://training.galaxyproject.org/training-material/topics/dev/tutorials/tool-from-scratch/tutorial.html#galaxy-tools-and-conda) issu du [Galaxy Training Network](https://training.galaxyproject.org/). (S'arrêter à la section *Galaxy Tool Wrappers*)

## Création de containers

Le but du jeu est de créer une image Docker et/ou Singularity contenant plusieurs paquets Conda.

Dans la vrai vie, il est possible de demander la création automatique d'images de ce type sur https://biocontainers.pro/multipackage.

### Création d'une image Docker

Il nous faut dans l'ordre :

- Créer un nouveau répertoire de travail
- Y créer un fichier Dockerfile ([cheat sheet](https://kapeli.com/cheat_sheets/Dockerfile.docset/Contents/Resources/Documents/index) pour la syntaxe)
- Se baser sur l'image [continuumio/miniconda3](https://hub.docker.com/r/continuumio/miniconda3), version 4.10.3
- Installer avec conda plusieurs logiciels de bioinfo: `biopython` en version 1.77, `samtools`, `bowtie2` et `bwa`
- Ajouter dans l'image [ce script](files/docker/test_script.sh) permettant de tester que les logiciels sont bien installés
  - Le placer dans /usr/local/bin/ avec les droits d'exécution
- Faire un build de l'image
- Lancer un container utilisant cette image, en montant un volume local
- Exécuter le script et écrire le résultat dans un nouveau fichier dans le volume monté

Variante possible : utiliser [mamba](https://github.com/mamba-org/mamba) pour créer l'environnement

### Création d'une image Singularity

Il nous faut dans l'ordre :

- Créer un nouveau répertoire de travail
- Y créer un fichier ma_super_image.def ([doc](https://sylabs.io/guides/3.8/user-guide/definition_files.html) pour la syntaxe)
- Se baser sur l'image [continuumio/miniconda3](https://hub.docker.com/r/continuumio/miniconda3), 4.10.3
- Installer avec conda plusieurs logiciels de bioinfo: `biopython` en version 1.77, `samtools`, `bowtie2` et `bwa`
- Ajouter dans l'image [ce script](files/docker/test_script.sh) permettant de tester que les logiciels sont bien installés
  - Le placer dans /usr/local/bin/ avec les droits d'exécution
- Faire un build de l'image
- Lancer un container utilisant cette image, en montant un volume local
- Exécuter le script et écrire le résultat dans un nouveau fichier dans le volume monté

Variante possible : utiliser [mamba](https://github.com/mamba-org/mamba) pour créer l'environnement


## Corrigé

Le corrigé est [là](corrigé.md), mais tricher, c'est mal.
