# Atelier reproductibilité

Les corrections

## Création d'une recette Conda

Le corrigé est directement dans [le tutoriel](https://training.galaxyproject.org/training-material/topics/dev/tutorials/tool-from-scratch/tutorial.html#galaxy-tools-and-conda) issu du [Galaxy Training Network](https://training.galaxyproject.org/). (S'arrêter à la section *Galaxy Tool Wrappers*)

## Création de containers

### Création d'une image Docker

[Dockerfile](files/docker/Dockerfile)

Commande pour le build:

```
docker build -t ma_super_image .
```

Commande pour le run:

```
docker run -it --rm -v ~/some_dir/:/data/:rw ma_super_image bash
test_script.sh > /data/out.txt
```

### Création d'une image Singularity

[Singularity definition file](files/singularity/ma_super_image.def)

Commande pour le build:

```
sudo singularity build ma_super_image.sif ma_super_image.def
```

Autre possibilité si sudo pas possible :

```
singularity build --fakeroot ma_super_image.sif ma_super_image.def
```

Commande pour le run:

```
docker run -it --rm -v ~/some_other_dir/:/data/:rw ma_super_image bash
test_script.sh > /data/out.txt
```
